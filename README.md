# SCSS PROJECT

## Getting started

1. VSCODE 확장 기능(**LiveSass Compiler**) 설치 <br />
<img src="./img/vscode1.png" alt="확장기능1" />

---

2. VSCODE 내에 Watch Sass 클릭하여 기능 활성화
<p style="max-width: 800px;"><img src="./img/vscode2.png" alt="확장기능2" /></p>
(클릭 전)

---

<p style="max-width: 800px;"><img src="./img/vscode3.png" alt="확장기능3" /></p>
(클릭 후)

---

3. scss 폴더 내부에 scss 파일 수정하면 css 폴더 내부에 css 파일이 생성 및 수정된다.
<p style="max-width: 800px;"><img src="./img/vscode4.png" alt="확장기능4" /></p>

## LiveSass Compiler Extension's Options

```json
{
  "liveSassCompile.settings.excludeList": [
    "**/node_modules/**",
    ".vscode/**",
  ],
  "liveSassCompile.settings.formats": [
    {
      "format": "compressed",
      "extensionName": ".css",
      "savePath": "~/../css"
    }
  ],
  "liveSassCompile.settings.generateMap": true,
  "liveSassCompile.settings.autoprefix": [
    "> 1%",
    "last 2 versions"
  ],
}
```

- liveSassCompile.settings.formats 옵션
  - 컴파일링 format: (nested, expanded, compact, compressed)
    - [SCSS 변환 스타일 설명](https://www.codingfactory.net/10083)
  - 컴파일링 savePath: (scss, css 폴더가 같을 경우 동일하게 설정 / ~: 현재 폴더)
- liveSassCompile.settings.generateMap: 컴파일링시 .map 파일을 생성한다. (크롬 sass 디버깅을 위해 필요)
- liveSassCompile.settings.autoprefix: 컴파일링 시 자동으로 미지원되는 css프로퍼티가 추가되어 저장된다. (ex: display: flex; -> display: -webkit-box; display: -ms-flexbox; display: flex; )

### LiveSass Compiler generateMap 추가 설명
- 위 설정으로 인하여 scss 파일 저장시 자동으로 css 폴더 내부에 .map 파일이 생성된다.

<p style="max-width: 800px;"><img src="./img/chrome2.png" alt="크롬 개발자" /></p>

---

- .map 파일 생성으로 인하여 크롬 개발자 도구로 scss 파일 확인이 가능해진다.

<p style="max-width: 800px;"><img src="./img/chrome1.png" alt="크롬 개발자" /></p>


### LiveSass Compiler @import 파일 추가설명
- import 되어야 하는 scss파일명 접두에 _를 추가해준다. _가 없을 시 LiveSass Compiler가 재시동되어야만 동작하고, 자동변환해주지 않는다.